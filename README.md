# Sample Flipping

Some calculations regarding how best to move samples in a grid to help avoid cross contamination.

Statement of problem:

> We're worried about contamination between adjacent samples, so ideally when they are run the second time, we'd maximize the distance between adjacent samples so that if there was contamination, it would be different between the replicates

Algorithm for moving samples suggested by Kevin Long:

![image](kevin_flip_image.jpg)